/*Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

 
**Example 1**:

```
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
```

**Example 2**:

```
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.
```

 

**Constraints**:

```
1 <= haystack.length, needle.length <= 104
haystack and needle consist of only lowercase English characters.
```
*/

//**My solution** :


class Solution {
    public int strStr(String haystack, String needle) {
        int index = 0, subStringEndPos = 0;
        if (needle.length() < 1) {
            index = 0;
        } 
        
        if (!haystack.contains(needle)) {
            index = -1;
        } else {
            for(int i = 0; i<needle.length(); i++) {
                for(int j = 0; j<haystack.length(); j++) {
                    if((j + needle.length()) <= haystack.length() &&
                      haystack.substring(j, j + needle.length()).equalsIgnoreCase(needle)) {
                        index = j;
                        break;
                    }
                } 
            }
        }            
        return index;
    }
}

